﻿using DealershipApp.DataAccess;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DealershipApp.Domain
{
    public class DomainHelper
    {
        public const double PI = 3.1415926535;
        DealerDB db = new DealerDB();
        public int Register(AccountDTO account, DealershipDTO dealership)
        {
            Dealer dealer = new Dealer();
            dealer.dealership.Address = dealership.Address;
            dealer.dealership.City = dealership.City;
            dealer.dealership.State = dealership.State;
            dealer.dealership.Latitude = Convert.ToDecimal(dealership.Latitude);
            dealer.dealership.Longitude = Convert.ToDecimal(dealership.Longitude);
            dealer.dealership.Name = dealership.Name;
            dealer.dealership.Phone = dealership.Phone;
            dealer.dealership.Zip = dealership.Zip;
            Debug.WriteLine(account.Username + account.Password);
            dealer.account.Username = account.Username;
            dealer.account.Password = account.Password;
            dealer.account.Email = account.Email;

            return db.Register(dealer);
        }

        public int Login(AccountDTO account)
        {
            Dealer dealer = new Dealer();
            dealer.account.Username = account.Username;
            dealer.account.Password = account.Password;
            return db.Login(dealer);
        }

        public bool SellVehicle(VehicleDTO vehicle)
        {
            Dealer dealer = new Dealer();
            dealer.vehicle.Color = vehicle.Color;
            dealer.vehicle.Condition = vehicle.Condition;
            dealer.vehicle.DealershipId = vehicle.DealershipId;
            dealer.vehicle.Make = vehicle.Make;
            dealer.vehicle.Mileage = vehicle.Mileage;
            dealer.vehicle.Model = vehicle.Model;
            dealer.vehicle.Price = vehicle.Price;
            dealer.vehicle.Year = vehicle.Year;
            dealer.vehicle.Type = vehicle.Type;
            return db.SellVehicle(dealer);
        }

        public IEnumerable<VehicleDTO> LotInfo(int id)
        {
            Dealer dealer = new Dealer();
            dealer.account.DealershipId = id;
            List<VehicleDTO> list = new List<VehicleDTO>();
            var vehicles = db.LotInfo(dealer);
            foreach(var item in vehicles)
            {
                VehicleDTO v = new VehicleDTO()
                {
                    DealershipId = item.DealershipId,
                    Make = item.Make,
                    Model = item.Model,
                    Year = item.Year,
                    Color = item.Color,
                    Condition = item.Condition,
                    Type = item.Type,
                    Price = item.Price,
                    Mileage = item.Mileage,
                    VehicleId = item.VehicleId
                };
                list.Add(v);
            }
            return list;

        }

        public DealershipDTO AccountInfo(int i)
        {            
            Dealer dealer = new Dealer();
            dealer.account.DealershipId = i;
            Dealership d = db.AccountInfo(dealer);
            return new DealershipDTO()
            {
                Address = d.Address,
                City = d.City,
                State = d.State,
                Zip = d.Zip,
                Name = d.Name,
                Phone = d.Phone
                
            };

        }


        public bool Remove(int id)
        {
            return db.Remove(id);
        }

        public IEnumerable<VehicleDTO> GetVehicles(VehicleDTO vehicle, int distance, double lat, double lon)
        {
            Debug.WriteLine("price" + vehicle.Price + vehicle.PriceMin + " " + vehicle.PriceMax);
            var sList = db.getVehicles();
            List<VehicleDTO> vList = new List<VehicleDTO>();
            foreach(var item in sList)
            {
                VehicleDTO v = new VehicleDTO()
                {
                    VehicleId = item.VehicleId,
                    DealershipId = item.DealershipId,
                    Make = item.Make,
                    Model = item.Model,
                    Year = item.Year,
                    Color = item.Color,
                    Condition = item.Condition,
                    Type = item.Type,
                    Price = item.Price,
                    Mileage = item.Mileage,
                    Latitude = System.Convert.ToDouble(item.Latitude),
                    Longitude = System.Convert.ToDouble(item.Longitude),
                    Address = item.Address,
                    City = item.City,
                    State = item.State,
                    Zip = item.Zip,
                    Phone = item.Phone,
                    Name = item.Name
                };
                vList.Add(v);                
            }
            Debug.WriteLine(vList);

            if (vehicle.Make != null)
            {
                Debug.WriteLine("filtering Make");
                vList = filterMake(vList, vehicle.Make).ToList();
            }

            if(vehicle.Model != null)
            {
                Debug.WriteLine("filtering model");
                vList = filterModel(vList, vehicle.Model).ToList();
            }

            if (vehicle.Year != null)
            {
                Debug.WriteLine("filtering year");
                vList = filterYear(vList, vehicle.Year).ToList();
            }

            if (vehicle.Color != null)
            {
                Debug.WriteLine("filtering color");
                vList = filterColor(vList, vehicle.Color).ToList();
            }

            if (vehicle.Condition != null)
            {
                Debug.WriteLine("filtering condition");
                vList = filterCondition(vList, vehicle.Condition).ToList();
            }

            if (vehicle.Type != null)
            {
                Debug.WriteLine("filtering type");
                vList = filterCondition(vList, vehicle.Type).ToList();
            }

            if(distance != -1 && distance!=0)
            {
                Debug.WriteLine("filtering distance " + distance + " " + lat + " " + lon);
                vList = filterDistance(vList, distance, lat, lon).ToList();
            }

            if(vehicle.PriceMin>=0 && vehicle.PriceMax >= 0)
            {
                if (vehicle.PriceMax != 0)
                {
                    Debug.WriteLine("filtering price");
                    vList = filterPrice(vList, vehicle.PriceMin, vehicle.PriceMax).ToList();
                }
            }

            Debug.WriteLine("returns " + vList);
            return vList;
        }

        private IEnumerable<VehicleDTO> filterMake(IEnumerable<VehicleDTO> vehicles, string make)
        {
            List<VehicleDTO> ret = new List<VehicleDTO>();
            foreach(var item in vehicles)
            {
                if (item.Make.Equals(make, StringComparison.InvariantCultureIgnoreCase))
                {
                    ret.Add(item);
                }
            }
            return ret;
        }

        private IEnumerable<VehicleDTO> filterModel(IEnumerable<VehicleDTO> vehicles, string model)
        {
            List<VehicleDTO> ret = new List<VehicleDTO>();
            foreach (var item in vehicles)
            {
                if (item.Model.Equals(model, StringComparison.InvariantCultureIgnoreCase))
                {
                    ret.Add(item);
                }
            }
            return ret;
        }

        private IEnumerable<VehicleDTO> filterYear(IEnumerable<VehicleDTO> vehicles, string year)
        {
            List<VehicleDTO> ret = new List<VehicleDTO>();
            foreach (var item in vehicles)
            {
                if (item.Year.Equals(year))
                {
                    ret.Add(item);
                }
            }
            return ret;
        }

        private IEnumerable<VehicleDTO> filterColor(IEnumerable<VehicleDTO> vehicles, string color)
        {
            List<VehicleDTO> ret = new List<VehicleDTO>();
            foreach (var item in vehicles)
            {
                if (item.Color.Equals(color, StringComparison.InvariantCultureIgnoreCase))
                {
                    ret.Add(item);
                }
            }
            return ret;
        }

        private IEnumerable<VehicleDTO> filterCondition(IEnumerable<VehicleDTO> vehicles, string condition)
        {
            List<VehicleDTO> ret = new List<VehicleDTO>();
            foreach (var item in vehicles)
            {
                if (item.Condition.Equals(condition))
                {
                    ret.Add(item);
                }
            }
            return ret;
        }

        private IEnumerable<VehicleDTO> filterType(IEnumerable<VehicleDTO> vehicles, string type)
        {
            List<VehicleDTO> ret = new List<VehicleDTO>();
            foreach (var item in vehicles)
            {
                if (item.Type.Equals(type))
                {
                    ret.Add(item);
                }
            }
            return ret;
        }

        private IEnumerable<VehicleDTO> filterPrice(IEnumerable<VehicleDTO> vehicles, float min, float max)
        {
            List<VehicleDTO> ret = new List<VehicleDTO>();
            foreach (var item in vehicles)
            {
                Debug.WriteLine("price" + item.Price + min + " " + max); 
                if (item.Price>=min && item.Price<=max)
                {
                    
                    ret.Add(item);
                }
            }
            return ret;
        }

        private IEnumerable<VehicleDTO> filterDistance(IEnumerable<VehicleDTO> vehicles, int radius, double lat, double lon)
        {
            List<VehicleDTO> ret = new List<VehicleDTO>();
            var latUser = lat * PI / 180;
            var lonUser = lon * PI / 180;
            foreach (var item in vehicles)
            {
                var latCar = item.Latitude * PI / 180;
                var lonCar = item.Longitude * PI / 180;    
                var dlat = lat - item.Latitude;
                var dlon = lon - item.Longitude;          
                var dist = Math.Acos(Math.Sin(latCar) * Math.Sin(latUser) + Math.Cos(latCar) * Math.Cos(latUser) * Math.Cos(lonCar - lonUser));
                dist *= 3959;
                if(dist < radius)
                {
                    ret.Add(item);
                }
            }
            return ret;
        }


    }
}
